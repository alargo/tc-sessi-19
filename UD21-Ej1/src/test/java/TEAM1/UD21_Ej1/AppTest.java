package TEAM1.UD21_Ej1;


import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import views.Principal;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void testSuma()
    {
        double resultado = Principal.operaciones(5, 5, "+");
        double esperado = 10;
        assertEquals(esperado, resultado);
        
    }
    
    @Test
    public void testResta()
    {
        double resultado = Principal.operaciones(6, 5, "-");
        double esperado = 1;
        assertEquals(esperado, resultado);
        
    }
    
    @Test
    public void testMult()
    {
        double resultado = Principal.operaciones(6, 5, "*");
        double esperado = 30;
        assertEquals(esperado, resultado);
        
    }
    
    
    @Test
    public void testDiv()
    {
        double resultado = Principal.operaciones(14, 2, "/");
        double esperado = 7;
        assertEquals(esperado, resultado);
        
    }
    
    @Test
    public void testRaiz()
    {
        double resultado = Principal.operaciones(9, 0, "Raiz");
        double esperado = 3;
        assertEquals(esperado, resultado);
        
    }
    
    @Test
    public void testElevado()
    {
        double resultado = Principal.operaciones(5, 0, "x2");
        double esperado = 25;
        assertEquals(esperado, resultado);
        
    }
    
    @Test
    public void testUnoDividido()
    {
        double resultado = Principal.operaciones(16, 0, "1/x");
        double esperado = 0.0625;
        assertEquals(esperado, resultado);
        
    }
    
    @Test
    public void testporcentaje()
    {
        double resultado = Principal.operaciones(15, 200, "%");
        double esperado = 30;
        assertEquals(esperado, resultado);
        
    }
    

    
}
