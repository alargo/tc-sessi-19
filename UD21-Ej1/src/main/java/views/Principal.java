package views;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.TextField;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.awt.event.ActionEvent;
import javax.swing.JTextArea;

public class Principal extends JFrame {

	private JPanel contentPane;
	private JTextArea panelcentral;
	private JTextArea historial;
	JButton porcentaje;
	JButton CE;
	JButton C;
	JButton borrar;
	JButton btnx;
	JButton siete;
	JButton cuatro;
	JButton uno;
	JButton masmenos;
	JButton potencia;
	JButton ocho;
	JButton cinco;
	JButton dos;
	JButton cero;
	JButton raiz;
	JButton nueve;
	JButton seis;
	JButton tres;
	JButton coma;
	JButton dividir;
	JButton multiplicar;
	JButton resta;
	JButton suma;
	JButton igual;
	
	 boolean guardarnumero = false;
	 double num1;
	 double num2;
	 String numeros ="";
	 String numerosoprimido = "";
	 String operacion = "";
	 double operador1 = 0;
	 double operador2 = 0;
	 double calculo = 0;
	 String nuevoNumero = "";
	 String resultado = "";
	
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Principal frame = new Principal();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Principal() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 752, 458);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		porcentaje = new JButton("%");
		porcentaje.setBounds(10, 206, 89, 23);
		contentPane.add(porcentaje);
		porcentaje.addActionListener(porcentajeN);
		
		CE = new JButton("CE");
		CE.setBounds(134, 206, 89, 23);
		contentPane.add(CE);
		CE.addActionListener(borrartodo);
		
		C = new JButton("C");
		C.setBounds(253, 206, 89, 23);
		contentPane.add(C);
		C.addActionListener(borrartodo);
		
		borrar = new JButton("Borrar");
		borrar.setBounds(375, 206, 89, 23);
		contentPane.add(borrar);
		borrar.addActionListener(borrartodo);
		
		btnx = new JButton("1/x");
		btnx.setBounds(10, 253, 89, 23);
		contentPane.add(btnx);
		btnx.addActionListener(divUno);
		
		siete = new JButton("7");
		siete.setBounds(10, 287, 89, 23);
		contentPane.add(siete);
		siete.addActionListener(añadirboton);
		
		cuatro = new JButton("4");
		cuatro.setBounds(10, 321, 89, 23);
		contentPane.add(cuatro);
		cuatro.addActionListener(añadirboton);
		
		uno = new JButton("1");
		uno.setBounds(10, 355, 89, 23);
		contentPane.add(uno);
		uno.addActionListener(añadirboton);
		
		masmenos = new JButton("+/-");
		masmenos.setBounds(10, 389, 89, 23);
		contentPane.add(masmenos);
		masmenos.addActionListener(negPos);
		
		panelcentral = new JTextArea();
		panelcentral.setBounds(10, 37, 454, 158);
		contentPane.add(panelcentral);
		
		historial = new JTextArea();
		historial.setBounds(487, 37, 225, 375);
		contentPane.add(historial);
		
		potencia = new JButton("x2");
		potencia.setBounds(134, 253, 89, 23);
		contentPane.add(potencia);
		potencia.addActionListener(potenciaN);
		
		ocho = new JButton("8");
		ocho.setBounds(134, 287, 89, 23);
		contentPane.add(ocho);
		ocho.addActionListener(añadirboton);
		
		cinco = new JButton("5");
		cinco.setBounds(134, 321, 89, 23);
		contentPane.add(cinco);
		cinco.addActionListener(añadirboton);
		
		dos = new JButton("2");
		dos.setBounds(134, 355, 89, 23);
		contentPane.add(dos);
		dos.addActionListener(añadirboton);
		
		cero = new JButton("0");
		cero.setBounds(134, 389, 89, 23);
		contentPane.add(cero);
		cero.addActionListener(añadirboton);
		
		 raiz = new JButton("Raiz");
		raiz.setBounds(253, 253, 89, 23);
		contentPane.add(raiz);
		raiz.addActionListener(raizN);
		
		 nueve = new JButton("9");
		nueve.setBounds(253, 287, 89, 23);
		contentPane.add(nueve);
		nueve.addActionListener(añadirboton);
		
		 seis = new JButton("6");
		seis.setBounds(253, 321, 89, 23);
		contentPane.add(seis);
		seis.addActionListener(añadirboton);
		
		 tres = new JButton("3");
		tres.setBounds(253, 355, 89, 23);
		contentPane.add(tres);
		tres.addActionListener(añadirboton);
		
		coma = new JButton(",");
		coma.setBounds(253, 389, 89, 23);
		contentPane.add(coma);
		coma.addActionListener(Coma);
		
		 dividir = new JButton("/");
		dividir.setBounds(375, 253, 89, 23);
		contentPane.add(dividir);
		dividir.addActionListener(div);
		
		 multiplicar = new JButton("*");
		multiplicar.setBounds(375, 287, 89, 23);
		contentPane.add(multiplicar);
		multiplicar.addActionListener(multi);
		
		 resta = new JButton("-");
		resta.setBounds(375, 321, 89, 23);
		contentPane.add(resta);
		resta.addActionListener(restar);
		
		 suma = new JButton("+");
		suma.setBounds(375, 355, 89, 23);
		contentPane.add(suma);
		suma.addActionListener(sumar);
		
		 igual = new JButton("=");
		igual.setBounds(375, 389, 89, 23);
		contentPane.add(igual);
		igual.addActionListener(resultados);
		
	}
	
	ActionListener añadirboton = new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			JButton botones = (JButton) e.getSource();
		
			if (!guardarnumero) {
				numeros = numeros + botones.getText();
				panelcentral.setText(numeros);
				operador1 = Double.parseDouble(numeros);
				System.out.println(operador1);
				
			}else {
				nuevoNumero = nuevoNumero + botones.getText();
				numeros = numeros + botones.getText();
				panelcentral.setText(numeros);
				operador2 = Double.parseDouble(nuevoNumero);
				System.out.println(operador2);
			}
			
		}
	};
	
	ActionListener borrartodo = new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			panelcentral.setText(null);
			
		}
	};
	
	
	ActionListener sumar = new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			guardarnumero = true;
			
			JButton botones = (JButton) e.getSource();
			numeros = numeros + botones.getText();
			
			panelcentral.setText(numeros);
			operacion = "+";
			
			
		}
	};
	
	ActionListener restar = new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			guardarnumero = true;
			
			JButton botones = (JButton) e.getSource();
			numeros = numeros + botones.getText();
			
			panelcentral.setText(numeros);
			operacion = "-";
			
			
		}
	};
	
	ActionListener multi = new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			guardarnumero = true;
			
			JButton botones = (JButton) e.getSource();
			numeros = numeros + botones.getText();
			
			panelcentral.setText(numeros);
			operacion = "*";
			
			
		}
	};
	
	ActionListener div = new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			guardarnumero = true;
			
			JButton botones = (JButton) e.getSource();
			numeros = numeros + botones.getText();
			
			panelcentral.setText(numeros);
			operacion = "/";
			
			
		}
	};
	
	ActionListener raizN = new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			guardarnumero = true;
			
			JButton botones = (JButton) e.getSource();
			numeros = numeros + botones.getText();
			
			panelcentral.setText(numeros);
			operacion = "Raiz";
			
			
		}
	};
	
	ActionListener potenciaN = new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			guardarnumero = true;
			
			JButton botones = (JButton) e.getSource();
			numeros = numeros + botones.getText();
			
			panelcentral.setText(numeros);
			operacion = "x2";
			
			
		}
	};

	ActionListener divUno = new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			guardarnumero = true;
			
			JButton botones = (JButton) e.getSource();
			numeros = numeros + botones.getText();
			
			panelcentral.setText(numeros);
			operacion = "1/x";
			
			
		}
	};
	
	ActionListener porcentajeN = new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			guardarnumero = true;
			
			JButton botones = (JButton) e.getSource();
			numeros = numeros + botones.getText();
			
			panelcentral.setText(numeros);
			operacion = "%";
			
			
		}
	};
	
	ActionListener negPos = new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			guardarnumero = true;
			
			JButton botones = (JButton) e.getSource();
			numeros = numeros + botones.getText();
			
			panelcentral.setText(numeros);
			operacion = "+/-";
			
			
		}
	};
	
	ActionListener Coma = new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			guardarnumero = true;
			
			JButton botones = (JButton) e.getSource();
			numeros = numeros + botones.getText();
			
			panelcentral.setText(numeros);
			operacion = ",";
			
			
		}
	};
	
	
	ActionListener resultados = new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			 calculo = operaciones(operador1, operador2, operacion );
			 System.out.println(operador1);
			 System.out.println(operador2);

			 
			 System.out.println(calculo);
			 
			 String calculoS = String.valueOf(calculo);
			 
			 panelcentral.setText(calculoS);
			 
			 resultado = resultado + String.valueOf(operador1) + operacion + String.valueOf(operador2) + "=" + calculoS + "\n";
			 
			 historial.setText(resultado);
			 
			 
			 panelcentral.setText("");
			 numeros = "";
			 nuevoNumero = "";
			 guardarnumero = false;

			
		}
	};
	
	public static double operaciones(double num1, double num2, String operacion) {
		double resultado=0.0;
		
		System.out.println(operacion);
		
		switch (operacion) {
		case "+":
			resultado = num1 + num2;
			break;
		case "-":
			resultado = num1 - num2;
			break;
		case "*":
			resultado = num1 * num2;
			break;
		case "/":
			resultado = num1 / num2;
			break;
		case "Raiz":
			resultado = Math.sqrt(num1);
			break;
		case "x2":
			resultado =  (int)Math.pow(num1, 2 );
			break;
		case "1/x":
			resultado =  1 / num1;
			break;
		case "%":
			resultado =  (num1*num2)/100;
			break;
		case "+/-":
			resultado =  num1 - (num1*2);
			break;

		default:
			break;
		}
		return resultado;
	}
	
	
	
	
}

